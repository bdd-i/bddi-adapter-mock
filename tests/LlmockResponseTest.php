<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Error;
use Luri\BddI\LowLevel\LlMockResponse;
use Luri\BddI\Common\SqlResponse;


class LlMockResponseTest extends TestCase {

	public function testForeach() {
		$res = new LlMockResponse();

		foreach ($res as $k=>$v) {
			switch ($k) {
				case 0:
					$this->assertEquals(1, $v['idsinger']);
					$this->assertEquals('Bars and Melody', $v['name']);
					$this->assertEquals('British', $v['nationality']);
					break;
				case 1:
					$this->assertEquals(2, $v['idsinger']);
					$this->assertEquals('Eddy De Pretto', $v['name']);
					$this->assertEquals('French', $v['nationality']);
					break;
				case 2:
					$this->assertEquals(3, $v['idsinger']);
					$this->assertEquals('Greyson Chance', $v['name']);
					$this->assertEquals('U.S.A.', $v['nationality']);
					break;
				case 3:
					$this->assertEquals(4, $v['idsinger']);
					$this->assertEquals('Troye Sivan', $v['name']);
					$this->assertEquals('Australian', $v['nationality']);
					break;
				case 4:
					$this->assertEquals(5, $v['idsinger']);
					$this->assertEquals('Ronan Parke', $v['name']);
					$this->assertEquals('British', $v['nationality']);
					break;
				default :
					$this->assertEquals(true, false, "Clé $k non reconnu dans la réponse");
			}
		}
	}

	public function testArrayAccess() {
		$res = new LlMockResponse();

		$this->assertEquals(1, $res[0]['idsinger']);
		$this->assertEquals('Bars and Melody', $res[0]['name']);
		$this->assertEquals('British', $res[0]['nationality']);

		$this->assertEquals(2, $res[1]['idsinger']);
		$this->assertEquals('Eddy De Pretto', $res[1]['name']);
		$this->assertEquals('French', $res[1]['nationality']);

		$this->assertEquals(3, $res[2]['idsinger']);
		$this->assertEquals('Greyson Chance', $res[2]['name']);
		$this->assertEquals('U.S.A.', $res[2]['nationality']);

		$this->assertEquals(4, $res[3]['idsinger']);
		$this->assertEquals('Troye Sivan', $res[3]['name']);
		$this->assertEquals('Australian', $res[3]['nationality']);

		$this->assertEquals(5, $res[4]['idsinger']);
		$this->assertEquals('Ronan Parke', $res[4]['name']);
		$this->assertEquals('British', $res[4]['nationality']);
	}

	public function testColumnsFilterWithBadArgument() {
		$this->expectException(\InvalidArgumentException::class);

		$res = new LlMockResponse();
		//Bad argument : must be an array. Will resturn InvalidArgumentException
		$res->setColumns('name');
	}

	public function testColumnsFilterWithColumnNotExist1() {
		$this->expectException(\OutOfBoundsException::class);

		$res = new LlMockResponse();
		//This column not exist in result. Will resturn OutOfRangeException
		$res->setColumns(['nom']);
	}

	public function testColumnsFilterWithColumnNotExist2() {
		$this->expectException(\OutOfBoundsException::class);

		$res = new LlMockResponse();
		//This column not exist in result. Will resturn OutOfRangeException
		$res->setColumns([50]);
	}

	public function testColumnsFilter() {
		$res = new LlMockResponse();

		//Return only one Column
		$res->setColumns(['name']);

		//test with ArrayAccess
		$this->assertArrayHasKey('name', $res[0]);
		$this->assertArrayNotHasKey('idsinger', $res[0]);
		$this->assertArrayNotHasKey('nationality', $res[0]);

		//test with foreach
		foreach ($res as $k=>$v) {
			$this->assertArrayHasKey('name', $v);
			$this->assertArrayNotHasKey('idsinger', $v);
			$this->assertArrayNotHasKey('nationality', $v);

			switch ($k) {
				case 0:
					$this->assertEquals('Bars and Melody', $v['name']);
					break;
				case 1:
					$this->assertEquals('Eddy De Pretto', $v['name']);
					break;
				case 2:
					$this->assertEquals('Greyson Chance', $v['name']);
					break;
				case 3:
					$this->assertEquals('Troye Sivan', $v['name']);
					break;
				case 4:
					$this->assertEquals('Ronan Parke', $v['name']);
					break;
				default :
					$this->assertEquals(true, false, "Clé $k non reconnu dans la réponse");
			}
		}

		//Return only two Column
		$res->setColumns(['idsinger','name']);

		//test with ArrayAccess
		$this->assertArrayHasKey('name', $res[0]);
		$this->assertArrayHasKey('idsinger', $res[0]);
		$this->assertArrayNotHasKey('nationality', $res[0]);

		//test with foreach
		foreach ($res as $k=>$v) {
			$this->assertArrayHasKey('name', $v);
			$this->assertArrayHasKey('idsinger', $v);
			$this->assertArrayNotHasKey('nationality', $v);

			switch ($k) {
				case 0:
					$this->assertEquals(1, $v['idsinger']);
					$this->assertEquals('Bars and Melody', $v['name']);
					break;
				case 1:
					$this->assertEquals(2, $v['idsinger']);
					$this->assertEquals('Eddy De Pretto', $v['name']);
					break;
				case 2:
					$this->assertEquals(3, $v['idsinger']);
					$this->assertEquals('Greyson Chance', $v['name']);
					break;
				case 3:
					$this->assertEquals(4, $v['idsinger']);
					$this->assertEquals('Troye Sivan', $v['name']);
					break;
				case 4:
					$this->assertEquals(5, $v['idsinger']);
					$this->assertEquals('Ronan Parke', $v['name']);
					break;
				default :
					$this->assertEquals(true, false, "Clé $k non reconnu dans la réponse");
			}
		}
	}

	public function testReturnIntIndex() {
		$res = new LlMockResponse();
		$res->setReturnIndex(SqlResponse::INDEXINT);

		//Test ArrayAccess
		$this->assertArrayHasKey(0, $res[0]);
		$this->assertArrayNotHasKey('idsinger', $res[0]);
		$this->assertArrayHasKey(1, $res[0]);
		$this->assertArrayNotHasKey('name', $res[0]);
		$this->assertArrayHasKey(2, $res[0]);
		$this->assertArrayNotHasKey('nationality', $res[0]);

		//Test foreach
		foreach ($res as $k=>$v) {
			$this->assertArrayHasKey(0, $v);
			$this->assertArrayNotHasKey('idsinger', $v);
			$this->assertArrayHasKey(1, $v);
			$this->assertArrayNotHasKey('name', $v);
			$this->assertArrayHasKey(2, $v);
			$this->assertArrayNotHasKey('nationality', $v);
		}
	}

	public function testReturnStringIndex() {
		$res = new LlMockResponse();
		$res->setReturnIndex(SqlResponse::INDEXSTRING);

		//Test ArrayAccess
		$this->assertArrayNotHasKey(0, $res[0]);
		$this->assertArrayHasKey('idsinger', $res[0]);
		$this->assertArrayNotHasKey(1, $res[0]);
		$this->assertArrayHasKey('name', $res[0]);
		$this->assertArrayNotHasKey(2, $res[0]);
		$this->assertArrayHasKey('nationality', $res[0]);

		//Test foreach
		foreach ($res as $k=>$v) {
			$this->assertArrayNotHasKey(0, $v);
			$this->assertArrayHasKey('idsinger', $v);
			$this->assertArrayNotHasKey(1, $v);
			$this->assertArrayHasKey('name', $v);
			$this->assertArrayNotHasKey(2, $v);
			$this->assertArrayHasKey('nationality', $v);
		}
	}

	public function testReturnIntAndStringIndex() {
		$res = new LlMockResponse();
		$res->setReturnIndex(SqlResponse::INDEXBOTH);

		//Test ArrayAccess
		$this->assertArrayHasKey(0, $res[0], 'le tableau suivant devrait avoir l\'index 0 : ' . print_r($res[0], true));
		$this->assertArrayHasKey('idsinger', $res[0]);
		$this->assertArrayHasKey(1, $res[0]);
		$this->assertArrayHasKey('name', $res[0]);
		$this->assertArrayHasKey(2, $res[0]);
		$this->assertArrayHasKey('nationality', $res[0]);

		//Test foreach
		foreach ($res as $k=>$v) {
			$this->assertArrayHasKey(0, $v);
			$this->assertArrayHasKey('idsinger', $v);
			$this->assertArrayHasKey(1, $v);
			$this->assertArrayHasKey('name', $v);
			$this->assertArrayHasKey(2, $v);
			$this->assertArrayHasKey('nationality', $v);
		}
	}

	public function testSearchValueInColumnUseBadColumnsName() {
		$this->expectException(\OutOfBoundsException::class);

		$res = new LlMockResponse();
		$line = $res->getLine('notexist', 'Ronan Parke');
	}

	public function testSearchValueInColumnUseBadColumnsNumber() {
		$this->expectException(\OutOfBoundsException::class);

		$res = new LlMockResponse();
		$line = $res->getLine(300, 'Ronan Parke');
	}

	public function testSearchValueInColumnUseColumnsName() {
		$res = new LlMockResponse();
		$line = $res->getLine('name', 'Ronan Parke');
		$this->assertEquals(
			[
				'idsinger' => 5,
				'name' => 'Ronan Parke',
				'nationality' => 'British',
			],
			$line
		);

		//WithOnlyidsingerColumn in result
		$res->setColumns(['idsinger']);
		$line = $res->getLine('name', 'Ronan Parke');
		$this->assertEquals(
			[
				'idsinger' => 5
			],
			$line
		);

		//With numerical index
		$res->setColumns(SqlResponse::ALLCOLUMNS);
		$res->setReturnIndex(SqlResponse::INDEXINT);
		$line = $res->getLine('name', 'Ronan Parke');
		$this->assertEquals(
			[ 5, 'Ronan Parke', 'British'],
			$line
		);

		//WithBothIndex
		$res->setReturnIndex(SqlResponse::INDEXBOTH);
		$line = $res->getLine('name', 'Ronan Parke');
		$this->assertEquals(
			[
				0 => 5,
				'idsinger' => 5,
				1 => 'Ronan Parke',
				'name' => 'Ronan Parke',
				2 => 'British',
				'nationality' => 'British',
			],
			$line
		);
	}

	public function testSearchValueInColumnButNotFound() {
		$this->expectException(\Luri\BddI\Common\NotFoundException::class);

		$res = new LlMockResponse();
		$res->getLine('name', 'Les Poppys');
	}

	public function testSearchValueInColumnUseColumnsNumber() {
		$res = new LlMockResponse();
		$line = $res->getLine(1, 'Ronan Parke');
		$this->assertEquals(
			[
				'idsinger' => 5,
				'name' => 'Ronan Parke',
				'nationality' => 'British',
			],
			$line
		);

		//WithOnlyidsingerColumn in result
		$res->setColumns(['idsinger']);
		$line = $res->getLine(1, 'Ronan Parke');
		$this->assertEquals(
			[
				'idsinger' => 5
			],
			$line
		);

		//With numerical index
		$res->setColumns(SqlResponse::ALLCOLUMNS);
		$res->setReturnIndex(SqlResponse::INDEXINT);
		$line = $res->getLine(1, 'Ronan Parke');
		$this->assertEquals(
			[ 5, 'Ronan Parke', 'British'],
			$line
		);

		//WithBothIndex
		$res->setReturnIndex(SqlResponse::INDEXBOTH);
		$line = $res->getLine(1, 'Ronan Parke');
		$this->assertEquals(
			[
				0 => 5,
				'idsinger' => 5,
				1 => 'Ronan Parke',
				'name' => 'Ronan Parke',
				2 => 'British',
				'nationality' => 'British',
			],
			$line
		);
	}

	public function testCount() {
		$res = new LlMockResponse();
		$this->assertEquals(5, count($res));
	}

	public function testCustomResponse() {
		$response = [
			['firstname' => 'Johnny', 'surname' => 'Orlando'],
			['firstname' => 'Lucas', 'surname' => 'Rieger'],
			['firstname' => 'Leondre', 'surname' => 'Devries']
		];
		$res = new LlMockResponse($response);

		//ArrayAccess
		$this->assertEquals('Leondre', $res[2]['firstname']);
		$this->assertEquals('Devries', $res[2]['surname']);

		//foreach
		foreach ($res as $k => $v) {
			switch ($k) {
				case 0:
					$this->assertEquals('Johnny', $v['firstname']);
					$this->assertEquals('Orlando', $v['surname']);
					break;
				case 1:
					$this->assertEquals('Lucas', $v['firstname']);
					$this->assertEquals('Rieger', $v['surname']);
					break;
				case 2:
					$this->assertEquals('Leondre', $v['firstname']);
					$this->assertEquals('Devries', $v['surname']);
					break;

			}
		}
	}

	public function testEmptyResponse() {
		$res = new LlMockResponse([]);

		$res = $res[0];
		$this->assertIsArray($res, print_r($res, true));
		$this->assertEmpty($res, print_r($res, true));
	}

	public function testOffsetNotExistWithoutError() {
		$res = new LlMockResponse();

		//Line 99 not exist
		$res = @$res[99];

		$this->assertEmpty($res, print_r($res, true));
	}

	public function testOffsetNotExistWithError() {
		$res = new LlMockResponse();

		//Line 99 not exist
		$this->expectException(Error::class);
		$res = $res[99];
	}
}
?>