<?php

/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 * ******************************************************************************
 *                                  LICENCE
 * ******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 * ******************************************************************************
 */

namespace Luri\BddI\LowLevel;

use Luri\BddI\Common\Db;
use Luri\BddI\Common\SqlRequest;
use Luri\BddI\Common\SqlResponse;

/**
 * Db Mock driver
 *
 * This driver is useful in test. It not send any request in a SQL server but it store request
 * and have a specific Standardised Response
 * Model : Httplug Mock Client by David de Boer <david@ddeboer.nl>, see  https://github.com/php-http/mock-client
 */
class LlMock implements Db {

	/**
	 * @var SqlRequest[]
	 */
	private $requests = [];

	/**
	 * @var SqlResponse[]
	 */
	private $responses = [];

	/**
	 * @var SqlResponse|null
	 */
	private $defaultResponse = null;

	/**
	 * @var Exception[]
	 */
	private $exceptions = [];

	/**
	 * @var Exception|null
	 */
	private $defaultException = null;

	/**
	 * Log of application
	 * @var string[]
	 */
	private $log = [];

	/**
	 * Array with corespondance between dbId and dbName
	 * key : dbId
	 * value : dbName
	 *
	 * @var array
	 */
	protected $dbname = [];

	/**
	 * Id of active database
	 * @var string
	 */
	protected $dbactive = "";

	/**
	 * retain constructor parameter
	 *
	 * 'server' => string,
	 * 'login' => string,
	 * 'password' => string,
	 * 'charset' => string
	 *
	 * @var Array
	 */
	protected $connection = [];

	/**
	 * {@inheritdoc}
	 */
	public function __construct($server, $login, $password, $charset) {
		$this->connection['server'] = $server;
		$this->connection['login'] = $login;
		$this->connection['password'] = $password;
		$this->connection['charset'] = $charset;
	}

	/**
	 * Return connection parameter passed to constructor
	 *
	 * Array format :
	 *
	 * 'server' => string,
	 * 'login' => string,
	 * 'password' => string,
	 * 'charset' => string
	 *
	 * @return array
	 */
	public function getConnectionParameter() : Array {
		return $this->connection;
	}

	/**
	 * {@inheritdoc}
	 */
	public function changeDb($dbId, $dbName = "") {
		if ($this->dbactive != $dbId) {
			//Active database is different than database requested, change.
			if (!empty($dbName)) {
				//Save this dbname
				$this->dbname[$dbId] = $dbName;
			} else {
				//No DbName, Search dnname
				if (empty($this->dbname[$dbId])) {
					//Not found
					throw new \BadFunctionCallException("Error : dbName not found for id : $dbId");
				}
				$dbName = $this->dbname[$dbId];
			}

			$this->log[] = "Change Db for $dbId => $dbName";

			//Save active databse
			$this->dbactive = $dbId;
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function exe(SqlRequest $sqlrequest): SqlResponse {
		//log
		$this->log[] = "Execute $sqlrequest";

		$this->requests[] = $sqlrequest;

		if (count($this->exceptions) > 0) {
			throw array_shift($this->exceptions);
		}
		if (count($this->responses) > 0) {
			return array_shift($this->responses);
		}
		if ($this->defaultException) {
			throw $this->defaultException;
		}
		if ($this->defaultResponse) {
			return $this->defaultResponse;
		}

		// Return default response
		return new LlMockResponse();
	}

	public function transactionStart() {
		$this->log[] = "Start Transaction";
	}

	public function transactionCommit() {
		$this->log[] = "Commit Transaction";
	}

	public function transactionRollback() {
		$this->log[] = "Rollback Transaction";
	}

	/////////////////////////////////////////////
	// HTTplug helper function (it's the same) //
	/////////////////////////////////////////////
	/**
	 * Adds an exception that will be thrown.
	 *
	 * @param \Exception $exception
	 */
	public function addException(\Exception $exception) {
		$this->exceptions[] = $exception;
	}

	/**
	 * Sets the default exception to throw when the list of added exceptions and responses is exhausted.
	 *
	 * If both a default exception and a default response are set, the exception will be thrown.
	 *
	 * @param \Exception|null $defaultException
	 */
	public function setDefaultException(\Exception $defaultException = null) {
		$this->defaultException = $defaultException;
	}

	/**
	 * Adds a response that will be returned.
	 *
	 * @param SqlResponse $response
	 */
	public function addResponse(SqlResponse $response) {
		$this->responses[] = $response;
	}

	/**
	 * Sets the default response to be returned when the list of added exceptions and responses is exhausted.
	 *
	 * @param SqlResponse|null $defaultResponse
	 */
	public function setDefaultResponse(SqlResponse $defaultResponse = null) {
		$this->defaultResponse = $defaultResponse;
	}

	/**
	 * Returns requests that were sent.
	 *
	 * @return SqlRequest[]
	 */
	public function getRequests() {
		return $this->requests;
	}

	/**
	 * @return SqlRequest|false
	 */
	public function getLastRequest() {
		return end($this->requests);
	}

	/**
	 * Return the last log
	 *
	 * @return string
	 */
	public function getLastLog() {
		return end($this->log);
	}

	/**
	 * Generator to have all Log
	 *
	 * Must use in a foreach statement
	 *
	 * @return array[]
	 */
	public function iterateAllLog() {
		yield from $this->log;
	}
}

?>