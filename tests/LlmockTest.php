<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

use PHPUnit\Framework\TestCase;
use Luri\BddI\LowLevel\LlMock;
use Luri\BddI\Common\SqlRequest;
use Luri\BddI\Common\SqlResponse;


class LlMockTest extends TestCase {

	/**
	 *
	 * @var LlMock;
	 */
	protected static $dbclass;

	/**
	 *
	 * @var ReflectionProperty
	 */
	protected static $property;


	public static  function setUpBeforeClass() {
		parent::setUpBeforeClass();

		self::$dbclass = new LlMock('on', 'sen', 'fiche', 'nongéré');

		//Reflexion helper to access $dbactive propriety
		self::$property = self::getPrivateProperty('\Luri\BddI\LowLevel\LlMock', 'dbactive' );
	}

	/**
 	 * getPrivateProperty
 	 *
 	 * @author	Joe Sexton <joe@webtipblog.com>
 	 * @param 	string $className
 	 * @param 	string $propertyName
 	 * @return	ReflectionProperty
 	 */
	public static function getPrivateProperty( $className, $propertyName ) {
		$reflector = new ReflectionClass( $className );
		$property = $reflector->getProperty( $propertyName );
		$property->setAccessible( true );

		return $property;
	}

	public function testInitialChangeDb() {
		//Change database (correct )
		self::$dbclass->changeDb('DBID1', 'dbname1');
		$this->assertEquals('DBID1', self::$property->getValue(self::$dbclass));
	}

	public function testChangeDbButDbNameUnknownMustThrownException() {
		$this->expectException(\BadFunctionCallException::class);
		//Change database (incorrect : no database name registered)
		self::$dbclass->changeDb('DBID2');
	}

	/**
	 * @depends testInitialChangeDb
	 */
	public function testChangeDbToDb2() {
		//Change database (correct)
		self::$dbclass->changeDb('DBID2', 'dbname2');
		$this->assertEquals('DBID2', self::$property->getValue(self::$dbclass));
	}

	/**
	 * @depends testChangeDbToDb2
	 */
	public function testChangeDbToDb1WithOnlyDbId() {
		//Change database (correct)
		self::$dbclass->changeDb('DBID1');
		$this->assertEquals('DBID1', self::$property->getValue(self::$dbclass));
	}


	public function testTransactionStart() {
		self::$dbclass->transactionStart();
		$this->assertEquals("Start Transaction", self::$dbclass->getLastLog());
	}

	public function testTransactionCommit() {
		self::$dbclass->transactionCommit();
		$this->assertEquals("Commit Transaction", self::$dbclass->getLastLog());
	}

	public function testTransactionRollback() {
		self::$dbclass->transactionRollback();
		$this->assertEquals("Rollback Transaction", self::$dbclass->getLastLog());
	}

	public function testDefaultExecute() {
			//Création du bouchon (Mock) simulant une vrai requète
			$request = $this->createMock(SqlRequest::class);
			$request->method('__tostring')->willReturn('SQL REQUEST #1');

			//test
			$res = self::$dbclass->exe($request);
			$this->assertInstanceOf(Luri\BddI\LowLevel\LlMockResponse::class, $res);
			$this->assertEquals('SQL REQUEST #1', self::$dbclass->getLastRequest());

	}


	public function testRequestWithDefaultResponse() {
		//Création du bouchon (Mock) simulant une vrai requète
		$request = $this->createMock(SqlRequest::class);
		$request->method('__tostring')->willReturn('SQL REQUEST #2');

		//Test
		$response = [
			['Johnny', 'Orlando'],
			['Lucas', 'Rieger'],
			['Leondre', 'Devries']
		];
		$defaultresponse = new \Luri\BddI\LowLevel\LlMockResponse($response);
		self::$dbclass->setDefaultResponse($defaultresponse);
		$res = self::$dbclass->exe($request);

		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMockResponse::class, $res);
		$this->assertEquals($defaultresponse, $res);
	}

	public function testgetRequests() {
		//On utilise une instance particulière pour ce test
		$llmock = new LlMock('on', 'sen', 'fiche', 'nongéré');

		//Création des bouchons (Mock) simulant udes vrais requètes
		$request1 = $this->createMock(SqlRequest::class);
		$request1->method('__tostring')->willReturn('SQL REQUEST #1');

		$request2 = $this->createMock(SqlRequest::class);
		$request2->method('__tostring')->willReturn('SQL REQUEST #2');

		//Test
		$llmock->exe($request1);
		$llmock->exe($request2);

		//Test getRequests
		$valtobetested = [];
		foreach ($llmock->getRequests() as $v) {
			$valtobetested[] = (string) $v;
		}

		$this->assertEquals(
			['SQL REQUEST #1','SQL REQUEST #2'],
			$valtobetested
		);
	}

	public function testaddResponse() {
		//Create request and response
		$sqlrequest = $this->createMock(SqlRequest::class);

		$response1 = [
			['Johnny', 'Orlando'],
			['Lucas', 'Rieger'],
			['Leondre', 'Devries']
		];
		$objectresponse1 = new \Luri\BddI\LowLevel\LlMockResponse($response1);
		$response2 = [
			['No Love (Like First Love)', 2018],
			['Defined', 2014],
			['Move', 2013],
			['We Are Shooting Stars', 2012],
			['A Thousand Miles', 2011]
		];
		$objectresponse2 = new \Luri\BddI\LowLevel\LlMockResponse($response2);

		//Test
		self::$dbclass->addResponse($objectresponse1);
		self::$dbclass->addResponse($objectresponse2);

		$res = self::$dbclass->exe($sqlrequest);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMockResponse::class, $res);
		$this->assertEquals($objectresponse1, $res);

		$res = self::$dbclass->exe($sqlrequest);
		$this->assertInstanceOf(Luri\BddI\LowLevel\LlMockResponse::class, $res);
		$this->assertEquals($objectresponse2, $res);
	}

	public function testDefaultException() {
		$this->expectException(\DomainException::class);

		$exception = new \DomainException("Oupps");

		self::$dbclass->setDefaultException($exception);
		$sqlrequest = $this->createMock(SqlRequest::class);
		$res = self::$dbclass->exe($sqlrequest);
	}

	public function testsetException1rstPart() {
		$this->expectException(\OverflowException::class);

		$exception = new \OverflowException("aie");
		self::$dbclass->addException($exception);
		$exception = new \UnexpectedValueException("Zoupla");
		self::$dbclass->addException($exception);

		$sqlrequest = $this->createMock(SqlRequest::class);
		$res = self::$dbclass->exe($sqlrequest);
	}

	/**
	 * @depends testsetException1rstPart
	 */
	public function testsetException2ndPart() {
		$this->expectException(\UnexpectedValueException::class);

		$sqlrequest = $this->createMock(SqlRequest::class);
		$res = self::$dbclass->exe($sqlrequest);
	}
}
?>