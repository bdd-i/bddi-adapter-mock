<?php
/**
 * This file is  Part of BDD-I
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * BDD-I is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\BddI\LowLevel;

use Luri\BddI\Common\SqlResponse;
use Luri\BddI\Common\NotFoundException;

/**
 * Represent an no SELECT, SHOW, DESCRIBE or EXPLAIN reponse
 *
 * This return :
 * For a SQL INSERT or UPDATE, this return auto generated id used in the latest query. If the modified
 * table does not have a column with the AUTO_INCREMENT attribute, this function will return zero.
 *
 * For other SQL query, this return equivalent of empty array
 *
 * Count : Return the number of affected rows.
 *
 */
class LlMockResponse implements SqlResponse {

	/**
	 * Value of response
	 * @var array
	 */
	protected $values = [];

	/**
	 * Column to be returned
	 *
	 * @var string
	 */
	protected $columnReturn = SqlResponse::ALLCOLUMNS;

	/**
	 * Type of index of result (int, string or both)
	 * @var int
	 */
	protected $returnIndex = SqlResponse::INDEXSTRING;

	/**
	 * Construct a new SqlResponse
	 *
	 * Response by default :
	 * --------------------------------------------
	 * | idsinger | name            | nationality |
	 * --------------------------------------------
	 * |    1     | Bars and Melody | British     |
	 * |    2     | Eddy De Pretto  | French      |
	 * |    3     | Greyson Chance  | U.S.A.      |
	 * |    4     | Troye Sivan     | Australian  |
	 * |    5     | Ronan Parke     | British     |
	 * --------------------------------------------
	 *
	 * @param array $values
	 */
	public function __construct($values = "") {
		if (is_array($values)) {
			if (!empty($values)) {
				$this->values = $values;
			} else {
				$this->values = [0 => []];
			}

		} else {
			//Default Response :
			//
			$this->values = [
				[
					'idsinger' => 1,
					'name' => 'Bars and Melody',
					'nationality' => 'British',
				],
				[
					'idsinger' => 2,
					'name' => 'Eddy De Pretto',
					'nationality' => 'French',
				],
				[
					'idsinger' => 3,
					'name' => 'Greyson Chance',
					'nationality' => 'U.S.A.',
				],
				[
					'idsinger' => 4,
					'name' => 'Troye Sivan',
					'nationality' => 'Australian',
				],
				[
					'idsinger' => 5,
					'name' => 'Ronan Parke',
					'nationality' => 'British',
				],
			];
		}
	}


	public function setColumns($columns = SqlResponse::ALLCOLUMNS) {
		//test
		if (!is_array($columns) AND $columns != SqlResponse::ALLCOLUMNS) {
			//Invalid data
			throw new \InvalidArgumentException("Parameter columns ($columns) is not valid");
		}

		if (is_array($columns)) {
			//Verify of all column exist in result
			foreach ($columns as $v) {
				if (is_int($v)) {
					//int
					$columnIndex = array_keys(reset($this->values));
					if (! array_key_exists($v, $columnIndex)) {
						throw new \OutOfBoundsException("Field $v not exist in result");
					}

				} else {
					//field name
					if (! array_key_exists($v, reset($this->values))) {
						throw new \OutOfBoundsException("Field $v not exist in result");
					}
				}
			}
		}

		//Valid Values, we store
		$this->columnReturn = $columns;
	}

	public function setReturnIndex($indextype = SqlResponse::INDEXSTRING) {
		if (
			$indextype != SqlResponse::INDEXSTRING AND
			$indextype != SqlResponse::INDEXINT AND
			$indextype != SqlResponse::INDEXBOTH
			) {
			//Error
			throw new \InvalidArgumentException("undefined indextype $indextype");
		}

		//save
		$this->returnIndex = $indextype;
	}

	/**
	 * Return only wanted column of a line of result with requested keys
	 *
	 * @param array $line A line of result.
	 * @return type
	 */
	protected function filter($line) {
		//column name in a array
		$columnIndex = $this->getcolumnIndex();


		if ($this->columnReturn == SqlResponse::ALLCOLUMNS) {
			//Want all column
			$colrequested = $columnIndex;
		} else {
			$colrequested = $this->columnReturn;
		}

		$ret= [];
		foreach ($colrequested as $colwanted) {
			if (is_int($colwanted)) {
				//index of field wanted
				switch ($this->returnIndex) {
					case SqlResponse::INDEXINT:
						//want only numeric key
						$ret[$colwanted] = $line[$columnIndex[$colwanted]];
						break;

					case SqlResponse::INDEXBOTH:
						//want both int and string keys
						$ret[$colwanted] = $line[$columnIndex[$colwanted]];
						$ret[$columnIndex[$colwanted]] = $line[$columnIndex[$colwanted]];
						break;

					case SqlResponse::INDEXSTRING:
					default:
						//want only string key
						$ret[$columnIndex[$colwanted]] = $line[$columnIndex[$colwanted]];
						break;
				}

			} else {
				//name of field wanted
				switch ($this->returnIndex) {
					case SqlResponse::INDEXINT:
						//want only numeric key
						//Retreive index of field
						$key = array_search($colwanted, $columnIndex);
						$ret[$key] = $line[$colwanted];
						break;

					case SqlResponse::INDEXBOTH:
						//want both int and string keys
						$key = array_search($colwanted, $columnIndex);
						$ret[$key] = $line[$colwanted];
						$ret[$colwanted] = $line[$colwanted];
						break;

					case SqlResponse::INDEXSTRING:
					default:
						//want only string key
						$ret[$colwanted] = $line[$colwanted];
						break;
				}
			}
		}

		return $ret;
	}

	/**
	 * REturn an array who countain column name
	 *
	 * @return array
	 */
	protected function getcolumnIndex() {
		$linetemp = current($this->values);
		if (!$linetemp) {
			$linetemp = reset($this->values);
		}
		return array_keys($linetemp);
	}

	public function getLine($col, $value) : array {
		//Is $col exist ?
		$columnIndex = $this->getcolumnIndex();

		if (is_int($col)) {
			//verif and search field name
			if (! array_key_exists($col, $columnIndex)) {
				throw new \OutOfBoundsException("Field $col not exist in result");
			}
			$col = $columnIndex[$col];
		} else {
			//only verif
			if (!in_array($col, $columnIndex)) {
				throw new \OutOfBoundsException("Field $col not exist in result");
			}
		}

		//key of searched value
		$key = array_search($value, array_column($this->values, $col));

		if ($key===false) {
			throw new NotFoundException("$value not found in column $col");
		}

		return $this->filter($this->values[$key]);
	}


	public function count() {
		return count($this->values);
	}


	public function current() {
		return $this->filter(current($this->values));
	}

	public function key() {
		return key($this->values);
	}

	public function next() {
		next($this->values);
	}

	public function rewind() {
		reset($this->values);
	}

	public function valid() {
		return (key($this->values) !== null);
	}



	public function offsetExists($offset) {
		return isset($this->values[$offset]);
	}

	public function offsetGet($offset) {
		if ($this->offsetExists($offset)) {
			//Normal situation
			return $this->filter($this->values[$offset]);
		} else {
			//Offset not exist, we don't want return result of $this->filter but an empty array with generate a php error
			$tabempty = [0 => []];
			return $tabempty[$offset+1];
		}
	}

	public function offsetSet($offset, $value) {
		$this->values[$offset] = $value;
	}

	public function offsetUnset($offset) {
		unset($this->values[$offset]);
	}

}
?>